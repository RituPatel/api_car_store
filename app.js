var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var AWS = require("aws-sdk");
const { Console } = require('console');

var app = express();
app.listen(3000, () => console.log('Cars API listening on port 3000!'))

AWS.config.update({
  region: "eu-west-2",
  endpoint: "http://localhost:8000"
});

var docClient = new AWS.DynamoDB.DocumentClient();

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.set('view engine', 'jade');

//start of api
app.get('/', function (req, res) {
  res.send({ title: "Cars API Entry Point" })
})


app.get('/cars', function (req, res) {
var params = {
    TableName: "Cars",
  };
console.log("Scanning Cars table.");
docClient.scan(params, onScan);
function onScan(err, data) {
    if (err) {
        console.error("Unable to scan the table. Error JSON:", JSON.stringify(err, null, 2));
    } else {
        res.send(data)
        console.log("Scan succeeded.");
        data.Items.forEach(function(car) {
           console.log(car.id, car.type, car.name)
        });
    }
  }
})

//get cars list-reading only some attributes from item
app.get('/carlist',(req,res)=>{
  console.log("printing cars list");
  var params ={
    TableName: "Cars",
    ProjectionExpression:"#id,#name",
    ExpressionAttributeNames:{
      "#id":"id",
      "#name":"name"
    }
  }
  console.log("ready to scan");
  docClient.scan(params,(err,data)=>{
    if(err){
      res.send("failed");
    }
    else{
      res.send(data)
    }
  });
});


app.get('/cars/:id', function (req, res) {
    var carID = parseInt(req.url.slice(6));
      console.log(req.url)
      console.log(carID)
    var params = {
          TableName : "Cars",

          KeyConditionExpression: "#id = :id",
          ExpressionAttributeNames:{
              "#id": "id"
          },
          ExpressionAttributeValues: {
              ":id": carID
          }
      };
    docClient.query(params, function(err, data) {
        if (err) {
            console.error("Unable to query. Error:", JSON.stringify(err, null, 2));
        } else {
            console.log("Query succeeded.");
            res.send(data.Items)
            data.Items.forEach(function(car) {
                console.log(car.id, car.name, car.type);
            });
        }
    });
    });


  app.get('/carDetails/:type',(req,res)=>{
    var carType = req.url.slice(12);
    console.log("carType="+carType);
    var params ={
      TableName:"Cars",
      Key:{
        type : carType
      }
    }

    docClient.scan(params,(err,data)=>{
      if(err){
        res.send("success:false");
      }
      else{
        res.send(data);
      }
    })
  });


//write item
app.post('/addcar',(req,res,next)=>{
  
const {id,name,type,manufacturer,fuel_type,description}=req.body;
var params = {
  TableName: "Cars",
    Item: {
        id: parseInt(id),
        name: name,
        type: type,
        manufacturer: manufacturer,
        fuel_type: fuel_type,
        description: description
    }
};

// Call DynamoDB to add the item to the table
docClient.put(params, function(err, data) {
  if (err) {
    res.send({
      success: false,
      message:'error:server error'
    });
    console.log("Error", err);
  } else {
    console.log("Successfully added car", data);
    res.send({
      success: true,
      message:'item added'
    });
  }
});   
});

//update item
app.patch('/carsupdate/:id',(req,res,next)=>{
  var carId = parseInt(req.url.slice(12));
  console.log("car id is:"+carId);
  const {name,type,manufacturer,fuel_type,description}=req.body;
  
  var params = {
    TableName:"Cars",
    Key:{
      id:carId
    },
    UpdateExpression:"set id = :i,name = :n,type = :t,manufacture = :m,fuel_type = :f,description = :d",
    ExpressionAttributeValues:{
      ':n':name,
      ':t': type,
      ':m': manufacturer,
      ':f': fuel_type,
      ':d': description
    },
    ReturnValues:"UPDATED_NEW"
  };
  console.log("updating item");
  docClient.update(params, function(err,data){
    if(err){
      res.send({
        success:false,
        message:'server error'
      });
      console.log(data);
    }else{
        console.log('data',data);
      res.send({
        success:true,
        message:'car updated',
        Cars:items
      });
    }
  });
});

//delete item
app.delete('/deletecar/:id',(req,res,next)=>{
  var carId=parseInt(req.url.slice(11));
  console.log("car id is:"+carId);
  var docClient=new AWS.DynamoDB.DocumentClient();

  var params={
    TableName:"Cars",
    Key:{
      id:carId
    }
  };
  console.log('deleting item');
  docClient.delete(params, function(err,data){
    if(err){
      console.error("unable to delete item");
      res.send({
        success:false,
        message:'Error:server error'
      });
    }else{
      console.log("item deleted");
      res.send({
        success:true,
        message:'car deleted',
      });
    }
  });
});


